from flask import Flask, render_template, redirect, request
from forms import SignUpForm

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mrjuoss-key'

@app.route('/')
def home():
    return 'Hello World'

@app.route('/about')
def about():
    return 'This is About Page'

@app.route('/blog')
def blog():
    return 'This is a blog'

@app.route('/blog/<int:blog_id>')
def post_detail(blog_id):
    return 'This is detail post with ID ' + str(blog_id)

@app.route('/product/<name>')
def get_product(name):
    return 'The product is ' + str(name)

@app.route('/template')
def template():
    return '''
    <html>
    <head>
        <title>Template Page</title>
    <head>
    <body>
        <h2>Welcome the blog</h2>
        <p>My Name is Jaki, Author this blog</p>
        <footer>
            Build with Love from Jakarta
        </footer>
    </body>
    </html>
    '''

@app.route('/post')
def post():
    posts = [{'title': "Technology 2020", 'author': 'Arif', 'body': 'Building website with Pyhton'},
             {'title': "Technology 2021", 'author': 'Mujaki', 'body': 'Building website with flask'},]
    return render_template('post.html', author="Mohamad Arif Mujaki", sunny=False, posts=posts)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignUpForm()
    if form.is_submitted():
        result = request.form
        return render_template('user.html', result=result)
    return render_template('signup.html', title='register', form=form)

if __name__ == '__main__':
    app.run()